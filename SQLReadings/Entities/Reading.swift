//
//  Reading.swift
//  Lab1SQL
//
//  Created by Pawel Srokowski on 19/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import Foundation

class Reading{
    
    var timestamp: Int64?
    var value: Float?
    var sensorId: Int32?
    
    init(timestamp: Int64, value: Float, sensorId: Int32) {
        self.timestamp = timestamp
        self.value = value
        self.sensorId = sensorId
    }
}
