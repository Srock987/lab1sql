//
//  Sensor.swift
//  Lab1SQL
//
//  Created by Pawel Srokowski on 19/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import Foundation
class Sensor{
    
    var id: Int32?
    var name: String?
    var desc: String?
    
    init(id: Int32, name: String, desc: String ) {
        self.id = id
        self.name = name
        self.desc = desc
    }
    

    
}
