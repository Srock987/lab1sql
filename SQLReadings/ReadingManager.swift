//
//  ReadingManager.swift
//  Lab1CoreData
//
//  Created by Pawel Srokowski on 15/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import Foundation
import SQLite3

class ReadingManager {
    
    
    
    static func fetchReadings() -> [Reading] {

        var fetchedReadings: [Reading] = []
        
        let db: OpaquePointer? = getDatabase()
        
        //this is our select query
        let queryString = "SELECT sensorId, value, timestamp FROM Readings;"
        
        //statement pointer
        var stmt:OpaquePointer?
        
        //preparing the query
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
        }
        
        //traversing through all the records
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let sensorId = sqlite3_column_int(stmt, 0)
            let value = Float(sqlite3_column_double(stmt, 1))
            let timestamp = sqlite3_column_int64(stmt, 2)
            
            let red: Reading = Reading(timestamp: timestamp, value: value, sensorId: sensorId)

            //adding values to list
            fetchedReadings.append(red)
        }
        return fetchedReadings
    }
    
    
    static func createReadings(number: Int){
        
        let db: OpaquePointer? = getDatabase()
        
        let createTableQuery = "CREATE TABLE IF NOT EXISTS Readings (sensorId INTEGER, value FLOAT, timestamp BIGINT);"
        if sqlite3_exec(db,createTableQuery,nil,nil,nil) != SQLITE_OK {
            print("Failed to create table")
            return
        }
        
        let insertQuery = "INSERT INTO Readings (sensorId, value, timestamp) VALUES (?,?,?)"
        let sensors: [Sensor] = SensorManager.fetchSensors()
        
        let beginQuery = "BEGIN TRANSACTION;"
        if sqlite3_exec(db, beginQuery, nil, nil, nil) == SQLITE_OK {
            print(beginQuery)
        }
    
        var stmt: OpaquePointer?
        
        for index in 1...number {
            let sensorId: Int32 = sensors[randomInt(max: sensors.count)].id!
            
            if sqlite3_prepare_v2(db, insertQuery, -1, &stmt, nil) != SQLITE_OK {
                print("Error binding query")
            }
            
            if sqlite3_bind_int(stmt, 1, sensorId) != SQLITE_OK {
                print("Error binding sensorId")
            }
            
            let value: Float = randomFloat()
            
            if sqlite3_bind_double(stmt, 2, Double(value)) != SQLITE_OK {
                print("Error binding value")
            }
            
            let timestamp: Int64 = randomDateFromYearBefore()
            
            if sqlite3_bind_int64(stmt, 3, timestamp) != SQLITE_OK {
                print("Error binding timestamp")
            }
            
            if sqlite3_step(stmt) != SQLITE_DONE {
                print("Error inserting")
            }
            
            sqlite3_finalize(stmt)
            
            if (index % 1000 == 0){
                print(index)
            }
            
        }
        
        let commitQuery = "COMMIT;"
        if sqlite3_exec(db, commitQuery, nil, nil, nil) == SQLITE_OK {
            print(commitQuery)
        }
        
        

    }
    
    static func deleteReadings() -> Int{
        let db: OpaquePointer? = getDatabase()

        let countQuery = "SELECT count(*) FROM Readings;"
        
        //statement pointer
        var stmt:OpaquePointer?
        
        
        //preparing the query
        if sqlite3_prepare_v2(db, countQuery, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
        }
        
        var rowsToDelete: Int32 = 0
        if sqlite3_step(stmt) != SQLITE_ROW {
            print("Error counting")
        } else{
            rowsToDelete = sqlite3_column_int(stmt, 0)
        }
        sqlite3_finalize(stmt)
        
        let beginQuery = "BEGIN TRANSACTION;"
        if sqlite3_exec(db, beginQuery, nil, nil, nil) == SQLITE_OK {
            print(beginQuery)
        }
        
        let sql = "DELETE FROM Readings;"
        if sqlite3_exec(db, sql, nil, nil, nil) != SQLITE_OK {
            print("Failed to delete")
        } else {
            print("All records deleted")
        }
        
        let commitQuery = "COMMIT;"
        if sqlite3_exec(db, commitQuery, nil, nil, nil) == SQLITE_OK {
            print(commitQuery)
        }
        
        return Int(rowsToDelete)
    }
    
    static func findFirstTimestamp(ascendingSort: Bool) -> Int64{
        var timeStamp: Int64 = 0
        let db: OpaquePointer? = getDatabase()
        
        var queryString: String
        //this is our select query
        if(ascendingSort){
            queryString = "SELECT min(timestamp) FROM Readings"
        } else {
            queryString = "SELECT max(timestamp) FROM Readings"
        }
        
        
        //statement pointer
        var stmt:OpaquePointer?
        
        //preparing the query
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
        }
        
        //traversing through all the records
        if sqlite3_step(stmt) == SQLITE_ROW {
            timeStamp = sqlite3_column_int64(stmt, 0)
            sqlite3_finalize(stmt)
        }
        
        return timeStamp
    }
    
    static func getDatabase() -> OpaquePointer? {
        var db: OpaquePointer?
        let fileUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("db.sqlite")
        
        if sqlite3_open(fileUrl.path, &db) != SQLITE_OK {
            print("Error opeining database")
        }
        return db
    }
    
    static func avrageValueForAllReadings() -> Float {
        
        var avrage: Float = 0
        
        let db: OpaquePointer? = getDatabase()
        
        //this is our select query
        let queryString = "SELECT avg(value) FROM Readings"
        
        //statement pointer
        var stmt:OpaquePointer?
        
        //preparing the query
        if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
        }
        
        if sqlite3_step(stmt) == SQLITE_ROW {
            avrage = Float(sqlite3_column_double(stmt, 0))
            sqlite3_finalize(stmt)
        }
        
        return avrage
    }
    
    static func avrageValueForEachSensor() -> [(key:String,value:Float)] {
        var avrages: [(key:String,value:Float)]  = []
        let sensors: [Sensor] = SensorManager.fetchSensors()
        let db: OpaquePointer? = getDatabase()
        
        for sensor in sensors {
            //this is our select query
            let queryString = String(format:"SELECT avg(value) FROM Readings WHERE sensorId=%d",sensor.id!)
            
            //statement pointer
            var stmt:OpaquePointer?
            
            //preparing the query
            if sqlite3_prepare_v2(db, queryString, -1, &stmt, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("error preparing insert: \(errmsg)")
            }
            
            var avrage: Float = 0
            
            if sqlite3_step(stmt) == SQLITE_ROW {
                avrage = Float(sqlite3_column_double(stmt, 0))
                sqlite3_finalize(stmt)
            }
            
            avrages.append((key: sensor.name!, value: avrage))
        }
        return avrages
    }
    
    static func randomDateFromYearBefore() -> Int64{
        let backInTime = Int64(arc4random_uniform(31556926))
        let currentTime = Int64(NSDate().timeIntervalSince1970)
        return currentTime - backInTime
    }
    
    static func randomInt(max: Int) -> Int{
        return Int(arc4random_uniform(UInt32(max)))
    }
    
    static func randomFloat() -> Float{
        return Float(arc4random()) / Float(UInt32.max) * 100
    }
}
