//
//  SensorTableViewController.swift
//  Lab1CoreData
//
//  Created by Pawel Srokowski on 15/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import UIKit
import CoreData

class SensorTableViewController: UITableViewController {
    // MARK: - Table view data source
    var sensors: [Sensor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    func loadData(){
        self.sensors = SensorManager.fetchSensors();
        self.tableView.reloadData()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sensors.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sensor = sensors[indexPath.row]
        let name = sensor.name
        let desc = sensor.desc
        let cell = tableView.dequeueReusableCell(withIdentifier: "SensorTableViewCell", for: indexPath) as! SensorTableViewCell
        cell.name?.text = name;
        cell.desc?.text = desc;
        return cell
    }
}
