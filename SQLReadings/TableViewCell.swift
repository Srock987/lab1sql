//
//  TableViewCell.swift
//  CoreData
//
//  Created by Pawel Srokowski on 15/12/2017.
//  Copyright © 2017 Pawel Srokowski. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
