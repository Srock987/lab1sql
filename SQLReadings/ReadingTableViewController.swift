//
//  RecordingTableViewController.swift
//  CoreData
//
//  Created by Pawel Srokowski on 15/12/2017.
//  Copyright © 2017 Pawel Srokowski. All rights reserved.
//

import UIKit
import CoreData

class ReadingTableViewController: UITableViewController {

    // MARK: - Table view data source
    var readings: [Reading] = []
    var sensors: [Sensor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    func loadData(){
        self.readings = ReadingManager.fetchReadings();
        self.sensors = SensorManager.fetchSensors()
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return readings.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reading = readings[indexPath.row]
        let sensor: Sensor = sensors.filter{ $0.id! == reading.sensorId }.first!
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReadingTableViewCell", for: indexPath) as! ReadingTableViewCell
        if let value: Float = reading.value  {
            cell.valueLabel?.text = String(value)
        }
        if let timestamp: Int64 = reading.timestamp {
            cell.timestampLabel?.text = String(timestamp)
        }
        if let name: String = sensor.name {
            cell.nameLabel?.text =  name
        }
        
        return cell
    }
 

}
