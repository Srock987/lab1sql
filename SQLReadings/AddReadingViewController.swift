//
//  AddRecordingViewController.swift
//  CoreData
//
//  Created by Pawel Srokowski on 15/12/2017.
//  Copyright © 2017 Pawel Srokowski. All rights reserved.
//

import UIKit
import CoreData

class AddReadingViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var result: UITextView!

    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBAction func addRecording(_ sender: Any) {
        guard let enteredText = self.input?.text else {
            return
        }
        
        if enteredText.isEmpty {
            self.result.text = "Set number of readings to create"
        } else {
            runExperiment(count: Int(enteredText)!)
        }
    }
    
    func setButtonInteractive(enabled: Bool){
        if(enabled){
            self.activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }else {
            self.activityIndicator.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    
    @IBAction func deleteReadings(_ sender: Any) {
        self.result.text = "Running..."
        setButtonInteractive(enabled: false)
        DispatchQueue(label: "Deleting").async{
            self.delReadings(completion: {(result) in
                self.result.text = result
                self.setButtonInteractive(enabled: true)
            })
        }

    }
    
    func delReadings(completion: @escaping(String) -> Void){
        let startTime = NSDate()
        let delCount = ReadingManager.deleteReadings()
        var resultString = "Deleted " + String(delCount) + " readings \n"
        let finishTime = NSDate()
        let measuredTIme = finishTime.timeIntervalSince(startTime as Date)
        resultString.append("It took " + String(measuredTIme) + "\n")
        DispatchQueue.main.async {
            completion(resultString)
        }
    }
    
    
    @IBAction func createThousandReadings(_ sender: Any){
        runExperiment(count: 1000)
    }
    
    @IBAction func createMilionReadings(_ sender: Any) {
        runExperiment(count: 1000000)
    }

    
    func runExperiment(count: Int){
        self.result.text = "Running..."
        setButtonInteractive(enabled: false)
        DispatchQueue(label: "Experiment").async {
            self.getResult(count:count,completion: { (result) in
                self.result.text = result
                self.setButtonInteractive(enabled: true)
            })
        }
    }
    
    func getResult(count: Int, completion: @escaping (String) -> Void){
        let startTime = NSDate()
    
        ReadingManager.createReadings(number: count)
        
        let biggestTimestamp = ReadingManager.findFirstTimestamp(ascendingSort: false)
        let smallestTimestamp = ReadingManager.findFirstTimestamp(ascendingSort: true)
        
        let avrageValue = ReadingManager.avrageValueForAllReadings()
        let avragesPerSensor: [(key:String,value:Float)] = ReadingManager.avrageValueForEachSensor()
        
        var resultString: String = ""
        resultString.append("Created " + String(count) + " readings \n")
        
        resultString.append("Biggest timestamp: \n" + String(biggestTimestamp) + "\n")
        resultString.append("Smallest timestamp: \n" + String(smallestTimestamp) + "\n")
        resultString.append("Avrage value of all: \n" + String(avrageValue) + "\n")
        resultString.append("Avrage value per sensor: \n")
        
        
        for sensorAvrage in avragesPerSensor{
            resultString.append(sensorAvrage.key + " : " + String(sensorAvrage.value) + " \n ")
        }
        
        let finishTime = NSDate()
        let measuredTIme = finishTime.timeIntervalSince(startTime as Date)
        resultString.append("It took " + String(measuredTIme) + "\n")
        DispatchQueue.main.async {
            completion(resultString)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        input.delegate = self
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    


}
