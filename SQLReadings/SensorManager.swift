//
//  SensorManager.swift
//  Lab1CoreData
//
//  Created by Pawel Srokowski on 15/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import Foundation
import SQLite3
class SensorManager{
    static func createSensors(number: Int){
        let db: OpaquePointer? = ReadingManager.getDatabase()
        
        let createTableQuery = "CREATE TABLE IF NOT EXISTS Sensors(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, desc TEXT)"
        
        if sqlite3_exec(db,createTableQuery,nil,nil,nil) != SQLITE_OK {
            print("Failed to create table")
            return
        }
        
        let insertQuery = "INSERT INTO Sensors (name, desc) VALUES (?,?)"
        

        
        for index in 1...number{
            var stmt: OpaquePointer?
            
            if sqlite3_prepare(db, insertQuery, -1, &stmt, nil) != SQLITE_OK {
                print("Error binding query")
            }
            
            let name: String = String(format: "S%02d",index)
            let desc: String = String(format:"Sensor number %02d",index)
            
            if sqlite3_bind_text(stmt, 1, name, -1, nil) != SQLITE_OK {
                print("Error binding sensorName")
            }
            
            if sqlite3_bind_text(stmt, 2, desc, -1, nil) != SQLITE_OK {
                print("Error binding sensorDesc")
            }
            
            if sqlite3_step(stmt) == SQLITE_DONE {
                print("Sensor inserted")
            }
        }

    }
    
    static func fetchSensors() -> [Sensor] {
        var fetchedSensors: [Sensor] = []
        
        let db: OpaquePointer? = ReadingManager.getDatabase()
        
        //this is our select query
        let queryString = "SELECT * FROM Sensors"
        
        //statement pointer
        var stmt:OpaquePointer?
        
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
        }
        
        //traversing through all the records
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let id = sqlite3_column_int(stmt, 0)
            let name = String(cString: sqlite3_column_text(stmt, 1))
            let desc = String(cString: sqlite3_column_text(stmt, 2))
            
            //adding values to list
            fetchedSensors.append(Sensor(id: id, name: name, desc: desc))
        }
        
        return fetchedSensors
    }
}
